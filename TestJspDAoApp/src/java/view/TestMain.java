
package view;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import model.*;

public class TestMain
{
    public static void main(String[] args) throws Exception
    {
        int cho;
        do
        {
        Scanner sc=new Scanner(System.in);
        System.out.println("--------menu---------");
        System.out.println("1. insert record");
        System.out.println("2. search record");
        System.out.println("3. Delete record");
        System.out.println("4. update record");
        System.out.println("5. search by pattern");
        System.out.println("6. exit");
        System.out.println("---------------------");
        System.out.println("enter your choice");
        int ch=sc.nextInt();
        if(ch==6)
        {
        System.exit(2);
        }
        else
           switch(ch) 
           {
               case 1:insert();break;
               case 2:search();break;
               case 3:delete();break;
               case 4:updateRecord();break;
               case 5:patternSearch();break;
           
               default:System.out.println("invalid choice");    
           }
            System.out.println("want to do more operations");
            System.out.println("1. yes");
            System.out.println("2. no");
             cho=sc.nextInt();
        }
        while(cho==1);
                {
                    System.out.println("--------THE END-------");
                }
    }
  

public static void insert() throws Exception
{
 Scanner sc=new Scanner(System.in);
    System.out.println("Enter the id");
    int id=sc.nextInt();
    System.out.println("Enter name");
    String name=sc.next();
    System.out.println("enter department");
    String dept=sc.next();
    System.out.println("Enter salary");
    int salary=sc.nextInt();
    System.out.println("Enter mobile number:");
    int phone=sc.nextInt();
    System.out.println("Enter email:");
    String email=sc.next();
    employee e=new employee();
    e.setDept(dept);
    e.setName(name);
    e.setId(id);
    e.setSalary(salary);
    e.setPhone(phone);
    e.setEmail(email);
    
    employeeDao ed=new employeeDao();
    if(ed.insertEmployee(e))
    {
        System.out.println("record inserted");
    }
    else
        System.out.println("record not inserted");  
}
public static void search() throws Exception
{
Scanner sc=new Scanner(System.in);
System.out.println("Enter the id you want to search");
int id=sc.nextInt();
employee e=new employee();
employeeDao ed=new employeeDao();
e=ed.searchById(id);
if(e==null)
{
    System.out.println("record not found");
}
else
{
    System.out.print(e.getId());
    System.out.print(" "+e.getName());
    System.out.print(" "+e.getDept());
    System.out.print(" "+e.getSalary());
    System.out.print(" "+e.getPhone());
    System.out.println(" "+e.getEmail());
} 
}      

public static void delete() throws Exception
{
Scanner sc=new Scanner(System.in);
    System.out.println("Enter the id you want to delete");
    int id=sc.nextInt();
    employeeDao ed=new employeeDao();
    if(ed.deleteRecord(id))
    {
        System.out.println("record deleted");
    }
    else
        System.out.println("record not found");
}

public static void updateRecord() throws Exception
{
    employee e=new employee();
Scanner sc=new Scanner(System.in);
    System.out.println("Enter the id you want to update");
    int id=sc.nextInt();
    employeeDao ed=new employeeDao();
    e=ed.searchById(id);
    if(e==null)
        System.out.println("record not found");
    else
    {  
    e.setName("shashank soni");
    e.setSalary(10000);
    }
    if(ed.updateEmployee(e))
        System.out.println("record updated");
    else
        System.out.println("record not found");
}
public static void searchAllRecord() throws Exception
{
employeeDao ed=new employeeDao();
List<employee> mylist1=new ArrayList <employee>();
mylist1=ed.searchAll();
for(employee e:mylist1)
{
    System.out.print(e.getId());
    System.out.print(" "+e.getName());
    System.out.print(" "+e.getDept());
    System.out.print(" "+e.getSalary());
    System.out.print(" "+e.getPhone());
    System.out.println(" "+e.getEmail());
}

} 

public static void patternSearch() throws Exception
{
Scanner sc=new Scanner(System.in);
    System.out.println("enter the character");
    String ch=sc.next();
    employeeDao ed=new employeeDao();
    List<employee> mylist1=new ArrayList <employee>();
    
    mylist1=ed.searchByPattern(ch);
    
    for(employee e:mylist1)
    {
    System.out.print(e.getId());
    System.out.print(" "+e.getName());
    System.out.print(" "+e.getDept());
    System.out.print(" "+e.getSalary());
    System.out.print(" "+e.getPhone());
    System.out.println(" "+e.getEmail());
    } 

}



}


