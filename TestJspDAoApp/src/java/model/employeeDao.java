
package model;
import connection.mycon;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class employeeDao 
{
  public boolean insertEmployee(employee e) throws Exception
  {
    Connection con=null;
    con =mycon.getConnection();
    String sql;
    sql="insert into employee values(?,?,?,?,?,?)";
    PreparedStatement ps=null;
    ps=con.prepareStatement(sql);
    ps.setInt(1,e.getId());
    ps.setString(2,e.getName());
    ps.setString(3,e.getDept());
    ps.setInt(4,e.getSalary());
    ps.setInt(5, e.getPhone());
    ps.setString(6,e.getEmail());
    if(ps.executeUpdate()>0)
        {
            return true;
        }
    else
        return false;
  }
  
  public employee searchById(int id) throws Exception
  {
    Connection con=null;
    con =mycon.getConnection();   
    String sql;
    sql="select * from employee where id=?";
    PreparedStatement ps=null;
    ResultSet rs=null;
    ps=con.prepareStatement(sql);
    ps.setInt(1,id);
    rs=ps.executeQuery();
    employee e=new employee();
  
    if(rs.next())
    {
        e.setId(rs.getInt(1));
        e.setName(rs.getString(2));
        e.setDept(rs.getString(3));
        e.setSalary(rs.getInt(4));
        e.setPhone(rs.getInt(5));
        e.setEmail(rs.getString(6));
    }
    else
      e=null;
    return e;
  }
  public boolean deleteRecord(int id) throws Exception
  {
    Connection con=null;
    con =mycon.getConnection();   
    String sql;
  
    sql="delete from employee where id=?";
    PreparedStatement ps=null;
    ps=con.prepareStatement(sql);
    ps.setInt(1,id);
    int n=ps.executeUpdate();
    if(n>0)
      return true;
    else
      return false;
  }
 
 public boolean updateEmployee(employee e) throws Exception 
 {
     Connection con=null;
    con =mycon.getConnection();
    String sql;
    sql="update employee set name=?,dept=?,salary=?,phone=?,email=? where id=?";
    PreparedStatement ps=null;
    ps=con.prepareStatement(sql);
    ps.setInt(6,e.getId());
    ps.setString(1,e.getName());
    ps.setString(2,e.getDept());
    ps.setInt(3,e.getSalary());
    ps.setInt(4, e.getPhone());
    ps.setString(5,e.getEmail());
  if(ps.executeUpdate()>0)
  {
  return true;
  }
  else
      return false;
 }     

    public List<employee> searchAll() throws Exception
 {
 String sql;
 PreparedStatement ps=null;
 ResultSet rs=null;
  Connection con=null;
  con =mycon.getConnection();
 sql="select * from employee";
 ps=con.prepareStatement(sql);
 List<employee> mylist=new ArrayList <employee>();
 rs=ps.executeQuery();
 while(rs.next())
 {
 employee e = new employee();
 e.setId(rs.getInt(1));
 e.setName(rs.getString(2));
 e.setDept(rs.getString(3));
 e.setSalary(rs.getInt(4));
 e.setPhone(rs.getInt(5));
 e.setEmail(rs.getString(6));
 mylist.add(e);
 }
 return mylist;
 }
 
 public List<employee> searchByPattern(String ch) throws Exception
 {
    String sql;
 PreparedStatement ps=null;
 ResultSet rs=null;
  Connection con=null;
  con =mycon.getConnection();
 sql="select * from employee where name like ?";
 ps=con.prepareStatement(sql);
 ps.setString(1, ch+"%");
 List<employee> mylist=new ArrayList <employee>();
 rs=ps.executeQuery();
 while(rs.next())
 {
 employee e = new employee();
 e.setId(rs.getInt(1));
 e.setName(rs.getString(2));
 e.setDept(rs.getString(3));
 e.setSalary(rs.getInt(4));
 e.setPhone(rs.getInt(5));
 e.setEmail(rs.getString(6));
 mylist.add(e);
 }
 return mylist;
 }
}


