/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import connection.mycon;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author shreya
 */
public class loginDao {
    
    
    public static boolean checkLogin(String uname,String upass) throws Exception
    {
    Connection con=null;
    con= mycon.getConnection();
    PreparedStatement ps=null;
    ResultSet rs=null;
    String sql;
    sql="select * from login where username=? and password=?";
    ps=con.prepareStatement(sql);
    ps.setString(1, uname);
    ps.setString(2, upass);
    rs=ps.executeQuery();
    if(rs.next())
    {
      return true;
    }
    else
        return false;
    }
}
