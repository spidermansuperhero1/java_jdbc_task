<%-- 
    Document   : index
    Created on : Oct 24, 2017, 6:29:59 PM
    Author     : shreya
--%>

<%@page import="model.loginDao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <center>
        <h1>Login page</h1>
        <form method="post" action="index.jsp">
        <table>
            
            <tr>
                <th>Name</th>
                <td><input type="text" name="txtname"></td>
            </tr>
            <tr>
                <th>Password</th>
                <td><input type="password" name="txtpass"></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="submit"></td>
                <td><input type="reset" value="reset"></td>
            </tr>
            </table>
        </form>
        </center>
    <%
         String uname,upass;
         uname=request.getParameter("txtname");
         upass=request.getParameter("txtpass");
         if(uname!=null)
         {
           if(loginDao.checkLogin(uname, upass))
           {
             session.setAttribute("uname", uname);
             session.setAttribute("upass", upass);
             response.sendRedirect("frontPagejsp.jsp");
           }
           else
           {
             out.println("invalid user name and password");
           }
         }
        
    %>
    </body>
</html>
